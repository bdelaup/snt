# 1 - Ajouter un fond uni aux sections 

###  Objectif
L'opjectif est d'obtenir ceci :

[![Objectif CSS 1](img/web/css-obj1.png){width=400}](img/web/css-obj1.png)

###  Démarche

1. Ouvrir les fichier `index.html` et `styles/style.css` du répertoire `8 - css - activite 1`
1. Ouvrir le fichier `index.html` du répertoire `8 - css - activite 1` avec notepad++ et un navigateur
2. Créer un nouveau fichier avec notepad++
3. Enregistrez le sous `styles/style.css`
4. Écrire un style pour le contenu des balise `<section>`:
[![Objectif css objectif 1](img/web/css-desc2.png){width=500}](img/web/css-desc2.png)
1. Dans le fichier `index.html`, ajouter une balise `<link>` dans la balise `<head>` et recharger la page dans le navigateur

!!! help "Astuce"
    Pour modifier les propriés d'une section, il faut ajouter les lignes suivants au fichier CSS.
    
    Chaque propriété est défini par un ensemble `clé : valeur`.
    
    Le code suivant appliquera un fond bleu à la zone de navigation. 
    
    ``` css
    nav {
        background : blue;
        border-style : solid;
    }
    ```



