# 6 - Ajouter un pied de page

### Objectif

[![Objectif html 6](img/web/html-obj6_v2.png){ width="300" }](img/web/html-obj6_v2.png) 


### Démarche
1. Ouvrir le fichier index.html du répertoire `6 - html - activite 6` avec notepad++ et un navigateur
2. Utiliser la balise d’emphase pour mettre en évidence les occurrences de *SNT*
3. Utiliser la balise de renforcement pour accentuer les occurrences d’*apprendre et de comprendre*

!!! help "Astuce"
    Regardez du coté des balises `<strong> ... </strong>` et `<em> ... </em>`

### A rédiger
1. :pencil2: Nommer la balise d’*emphase*
2. :pencil2: Nommer la balise de **renforcement**


