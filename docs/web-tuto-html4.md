# 4 - Ajouter des liens hypertextes

### Objectif

[![Objectif html 3](img/web/html-obj4.png)](img/web/html-obj4.png)


### Démarche

1. Ouvrir le fichier index.html du répertoire 4 - html - activite 4 avec notepad++ et un navigateur
2. Ajouter un lien nommé 
   <br> Wikipedia :arrow_right: [https://www.wikipedia.org](https://www.wikipedia.org) 
   <br> Mon lycée :arrow_right: site du lycée Jules Haag

!!! help "Astuce"
    Utiliser la balise `<a>`.
    
    === "code"
        ``` html
        <a href="http://wikipedia.org">Texte du lien à afficher</a>
        ```
    === "Rendu"              
        <a href="http://wikipedia.org">Texte du lien à afficher</a>
        

### A rédiger

1. :pencil2: Identifier la balise utilisée pour créer un lien hypertexte
2. :pencil2: Que ce passe-t-il si l’on clique sur le lien ?
3. :pencil2: En quoi la balise `<a>` est-elle fondamentale à la construction du WEB ?
4. :pencil2: Nommer l’attribut utiliser pour définir la ressource vers laquelle dirigé l’utilisateur ?


