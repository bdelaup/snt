# 4 – Un peu de maquillage

###  Objectif
L'opjectif est d'obtenir ceci :

[![Objectif CSS 2](img/web/css-obj4-full.png){width=600}](css-obj4-full)

###  Démarche
1. Ouvrir les fichier `index.html` et `styles/style.css` du répertoire `11 - css - activite 4`
2. Modifier la police de tout le texte contenu dans les sections
    ``` css hl_lines="3" 
    section {
        ...
        font-family: Verdana, Geneva, Tahoma, sans-serif;
    }
    ```
3. Modifier la couleur d’arrière plan du corps de la page pour un rouge Bordeau foncé

    !!! help "Astuce"
        La couleur bordeau rouge foncé peut être obtenu par le code RVB `rgb(30, 10, 10)`
