# 3 – Un peu de maquillage

###  Objectif
L'opjectif est d'obtenir ceci :

[![Objectif CSS 2](img/web/css-obj-3a.png){width=600}](img/web/css-obj-3a.png)

###  Démarche
1. Ouvrir les fichier `index.html` et `styles/style.css` du répertoire `10 - css - activite 3`
2. Fixer la largeur du bloc principal à **700 pixels** et **centré**
3. Compléter le bloc main avec les propriétés `width`, `margin-left`, `margin-right`

    ``` css hl_lines="6 7 8"
    main {
        background-color: lightgrey;
        border-color: darkgrey;
        border-width: 3px;
        border-style: solid;
        width: 700px;
        margin-left: auto;
        margin-right: auto;
    }
    ``` 


4. Mettre une image en arrière plan de l’entête

    ``` css 
    header {
        background-image: url('../images/snt.jpg');
        background-size: cover;
        margin: 5px;
        padding: 5px;
    }
    ```
