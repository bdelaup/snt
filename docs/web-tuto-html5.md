# 5 - Ajouter des liens hypertextes

### Objectif

[![Objectif html 5](img/web/html-obj5.png){ width="500" }](img/web/html-obj5.png) 


### Démarche

1. Ouvrir le fichier index.html du répertoire `5 - html - activité 5` avec Notepad++ et un navigateur
2. Ajouter un pied de page :arrow_right: contenant  un paragraphe :arrow_right: contenant le texte "*Creative common*"
 
``` html
    <footer> ...   </footer>
              |
              <p>...</p>
                   |
                   Creative common 
```

