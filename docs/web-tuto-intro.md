# Introduction

Lorsque l'on écrit une page web on dissocie le **fond** et la **forme**.

**Le fond** : c'est ce que vous voulez publier, image, video ...
**La forme** : c'est l'apparence, la mise en page, taille de la police ...

Les document concernant le fond, sont des fichier portant l'extension `.html` ou `.htm`. Ce sont les pages web.
Les documents concernant la forme, sont des fichier portant l'extension `.css`. Ce sont les feuilles de styles. 

Pour éditer les fichiers, nous vous proposons d'utiliser le logiciel `notepad++`.

Pour visualiser vos page, nous vous proposons d'utiliser le navigateur web `firefox`.

Voici une illustration. Dans les deux cas le fond est le même : c'est le même fichier *HTML*. Mais dans le second cas on applique une feuille de style pour changer la mise en page.

=== "Page sans feuille de style "
    [![Structure HTML]( img/web/rendu_sans_css.png){ width="450" } ]( img/web/rendu_sans_css.png)
=== "Page avec une feuille de style"
    [![Structure HTML]( img/web/rendu_avec_css.png){ width="450" } ]( img/web/rendu_avec_css.png)


Dans le tutoriel, nous allons vous allez apprendre à écrire un document en utilisant le langage HTML.
Puis vous apprendrez à le mettre en forme en créant une feuille de style.

!!! Warning 
    **Avant de commencer**, copier le contenu du répertoire d'activité présent dans votre répertoire vers votre espace personnel.<br> De `t:\snt\snt_delaup\0-web` vers `P:\documents\snt\`
    


