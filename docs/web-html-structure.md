# Structure d'une page web
[![Structure HTML]( img/web/web.png) ]( img/web/web.png) 


=== "Rendu"
    [![Structure HTML]( img/web/page_html.png){ width="450" } ]( img/web/page_html.png)

=== "Code"
    ``` html
    <!DOCTYPE html>
    <html>

    <head>
        <title>Titre de l'onglet</title>
        <meta charset="utf-8" />
        <link rel="stylesheet" type="text/css" href="styles/style.css">
    </head>

    <body>
        <p><b>Body : corps du document</b></p>
        <header>
            <h3>Header : entête du document</h3>
            <p> ex : Titre de la page</p>
        </header>
        <nav>
            <h3>Nav : menu</h3>
            <ul>
                <li>Lien page 1</li>
                <li>Lien page 2</li>
            </ul>
        </nav>
        <main>
            <p><b>Main : Zone principale du document</b></p>
            <section>
                <h3>Section : section 1</h3>
                <p>ex : première partie du document</p>
            </section>
            <section>
                <h3>Section : section 2</h3>
                <p>ex : deuxième partie du document</p>
            </section>
            <section>
                <h3>Section : section 3</h3>
                <p>ex : troisième partie du document</p>
            </section>
        </main>
        <footer>
            <h3>Footer : pied du document</h3>
            <p>Exemple : Auteur</p>
        </footer>
    </body>

    </html>
    ``` 

