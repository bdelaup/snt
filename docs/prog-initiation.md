
# Initiation à ma programmation
##  Carte mentale

![](img/prog-initiation/image2.jpeg){width="600"}

##  Du problème aux résultats

![](img/prog-initiation/image3.png){width="600"}

##  Définitions

**^^Algorithme^^** : c'est une **suite** finie et non ambiguë
**d'opérations** **permettant** de **résoudre** une **classe** de
**problèmes**

**^^Programme^^** : c'est

1.  Une **suite d'instruction**

2.  Écrite dans un langage **compréhensible par un humain**

3.  En vue de le transformer dans un langage qu'**un ordinateur pourra
    interpréter**

4.  Dans le but de réaliser une **tâche** **complexe** ou
    **répétitive**.

**^^Structure algorithmique^^** : il existe quatre opérations
fondamentales permettant d'**exprimer un algorithme** :

-   Les **instructions**

-   Les **variables**

-   Les **boucles** (ex : faire tant que...)

-   Les **branchements conditionnels** (si ... alors ... sinon)

[Langage de programmation]^^** : c'est une **manière de décrire
un algorithme en vue de le faire exécuter par un ordinateur**. Chaque
langage possède une syntaxe qui lui est propre. Chaque programme est
écrit dans un langage particulier. Exemple : Javascript, Python, Scratch

[Code source d'un programme]^^** **: fichier texte contenant le
programme**, éditable avec un éditeur de texte et appliquant les règles
d'écriture d'un langage de programmation

**^^Compilateur^^** : logiciel informatique utilisé pour
**transformer un programme en langage machine** (compréhensible par un
ordinateur).

**^^Interpréteur^^** : logiciel informatique permettant de **lire
un programme et d'exécuter** les **instructions au fur et à mesure**
qu'elles sont rencontrées par l'interpréteur

**^^Environnement de développement^^** : programme utiliser par
les développeurs d'application. Il contient généralement.

-   Un éditeur de texte avec un coloration syntaxique

-   Un compilateur ou un interpréteur

-   Des outils facilitant l'écriture du code.

Exemple : Thonny, Edupython
