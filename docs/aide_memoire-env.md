---
marp : true
---

# Les ressources de SNT
## Un site web
[https://bdelaup.gitlab.io/snt/](https://bdelaup.gitlab.io/snt/)

--- 
## Le réseau du lycée

---
### Espace documentaire commun

``` cmd title="Commun pédagogie" 

.
└── 📁 T:\commun pedagogie
    └── 📁snt
        ├── 📁 SNT_delaup
        │   ├── 📁0 - python
        │   │   ├── 📝python-Scratch_Python.pdf
        │   │   ├── 📝python-structure de controle - eleve.pdf
        │   │   ├── 📝python-turtle_niveau2
        │   │   └── 📝python_algo - synthese.pdf
        │   ├── 📁1 - internet
        │   ├── 📁2 - web
        │   ├── 📁3 - donnee_traitement
        │   ├── 📁4 - photo
        │   ├── 📁5 - reseau_sociaux
        │   ├── 📁6 - geolocalisation
        │   └── 📁7 - objet_connecte
        ├── 📁logiciels
        │   ├── 📁thonny
        │   └── 📁edupython
        └── 📁 SNT_lezais
```

---

### Espace personnel
``` cmd title="Espace personnel"
.
├── .
└── 📁 P:\prenom.nom
    ├── 📁documents
    │   └── 📁snt
    │       ├── 📁0 - python
    │       │   ├── 📝python-Scratch_Python.pdf
    │       │   ├── 📝python-structure de controle - eleve.pdf
    │       │   ├── 📝python-turtle_niveau2
    │       │   └── 📝python_algo - synthese.pdf
    │       └── 📁logiciels
    │           └── 📁thonny
    └── 📁bureau
```
---
### Un espace partagé
``` cmd title="Espace partagé"
.
# 📁 ?:\ma classe
## 📁 Documents en consultation
## 📁 Echange
```

