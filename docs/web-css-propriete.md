# Propriété du CSS
[![Structure HTML]( img/web/css.png) ]( img/web/css.png)

### Mise en forme de la police de caractère

*Exemple :*
``` css
body {
    border-color: red;
    border-width: 10px;
    border-radius: 10px;
    border-style: solid;
    background-color: lightcyan;
}
```

### Mise en forme des blocs :
| Propriété        | Fonction                  | Valeurs possibles          |
| ---------------- | ------------------------- | -------------------------- |
| border-color     | Couleur de   bordur       | red                        |
| border-width     | Epaisseur de   la bordure | Bold                       |
| border-radius    | Coin arrondi              | 10px                       |
| border-style     | Tirets                    | dashed                     |
| --               | Pointillés                | dotted                     |
| --               | Continu                   | solid                      |
| background-color | Couleur   d’arrière-plan  | lightcyan                  |
| background-image | Image d’arrière   plan    | url("images/monimage.jpg") |
| background-size  | --                        | cover                      |


### Dimensionnement des blocs

*Exemple :*
``` css
nav {
    height: 100px;
    margin: 5px;
    padding: 5px;
    width: 700px;
}
```
[![Structure HTML]( img/web/marges.png){ width="400" } ]( img/web/marges.png)

| Propriété | Fonction             | Valeurs possibles                                                                         |
| --------- | -------------------- | ----------------------------------------------------------------------------------------- |
| height    | Hauteur              | 100px                                                                                     |
| width     | Largeur              | 700px                                                                                     |
| padding   | Marge interne        | 5px                                                                                       |
| margin    | Marge externe        | 5px                                                                                       |
| margin        | Largeur fixé, centré | width: 700px; <br>  margin-left: auto; <br> margin-left: auto; <br>   margin-right: auto; |


