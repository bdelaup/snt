# 2 - Ajouter du contenu

### Objectif

[![Objectif html 2](img/web/html-obj2.png)](img/web/html-obj2.png)


### Démarche

1. Ouvrir le fichier index.html du répertoire `2 - html - activite 2` avec notepad++ et un navigateur
2. Modifier le titre pour que s’afficher ***2 – Ajouter du contenu*** dans l’onglet
3. Créer une seconde section contenant la description du thème web.
=== "Code"
    ``` html
    <section>
        <h2>--------------</h2>
        <h3>--------------</h3>

        <p>--------------</p>
        <h3>--------------</h3>
        <ul>
            <li>--------------</li>
            <li>--------------</li>
            <li>--------------</li>
        </ul>
    </section>
    ``` 
=== "Rendu"
    ![Rendu html 2](img/web/html-rendu2.png)
 

### A rédiger

??? help "Astuce"
    Utiliser les aides mémoire !

1. :pencil2: Quel est la différence entre les balises `<h2>` et `<h3>`
2. :pencil2: Quel balise sert à délimiter les différentes parties de la page ?
3. :pencil2: A quoi servent les balises `<ol>`, `<ul>` et `<li>` ?


