# 2 – Ajouter d’autres fonds unis 

###  Objectif
L'opjectif est d'obtenir ceci :

[![Objectif CSS 2](img/web/css-obj2-full.png){width=600}](img/web/css-obj2-full.png)

###  Démarche
1. Ouvrir les fichier `index.html` et `styles/style.css` du répertoire `9 - css - activite 2`
2. Compléter la feuille de style comme ci-dessous




=== "main"

    ``` css
    main {
        background-color: white;
        border-color: white;
        border-width: 3px;
        border-style: solid;
    }
    ```

=== "nav"

    ``` css
    nav {
        background-color: lightseagreen;
        margin: 5px;
        padding: 5px;
        border-radius: 10px;
    }
    ```

=== "footer"

    ``` css
    footer {
        background-color: white;
        padding: 0px;
        margin: 0px;
        border-radius: 0px;
    }
    ```


