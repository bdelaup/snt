# Présentation

Vous réaliserez une page web sur le des réseaux sociaux.


<p style="font-size: 0.9rem;font-style: italic;"><img style="display: block;" src="https://live.staticflickr.com/2060/2048034334_22b098c829_b.jpg" alt="My Twitter Social Ego Networks"><a href="https://www.flickr.com/photos/11452351@N00/2048034334" target="_blank" rel="noopener noreferrer">"My Twitter Social Ego Networks"</a><span> by <a href="https://www.flickr.com/photos/11452351@N00" target="_blank" rel="noopener noreferrer">David Sousa-Rodrigues</a></span> is licensed under <a href="https://creativecommons.org/licenses/by-nc-sa/2.0/&atype=html" style="margin-right: 5px;" target="_blank" rel="noopener noreferrer">CC BY-NC-SA 2.0</a><a href="https://creativecommons.org/licenses/by-nc-sa/2.0/&atype=html" target="_blank" rel="noopener noreferrer" style="display: inline-block;white-space: none;margin-top: 2px;margin-left: 3px;height: 22px !important;"><img style="height: inherit;margin-right: 3px;display: inline-block;" src="https://search.openverse.engineering/static/img/cc_icon.svg?media_id=b3ab515a-504e-4c66-a8c8-1d36a64c162a" /><img style="height: inherit;margin-right: 3px;display: inline-block;" src="https://search.openverse.engineering/static/img/cc-by_icon.svg" /><img style="height: inherit;margin-right: 3px;display: inline-block;" src="https://search.openverse.engineering/static/img/cc-nc_icon.svg" /><img style="height: inherit;margin-right: 3px;display: inline-block;" src="https://search.openverse.engineering/static/img/cc-sa_icon.svg" /></a></p>

    


