# 1 - Ma première page

###  Objectif
L'opjectif est d'obtenir ceci :

![Image title](img/web/firefox_court.png){ width="150"}


###  Démarche
2. Ouvrir le répertoire `1 – html – activité 1 `
>![Image title](img/web/tree_tuto_court.png){ width="150"}
   
3. Ouvrir notepad++
>![Image title](img/web/npp_court.png){ width="150"}

4. Recopier le code ci-dessous
    ``` html
    <!DOCTYPE html>
    <html>

        <head>
            <title>1-Première page</title>
        </head>

        <body>
            <h1>Bienvenue en SNT</h1>
            <p>Voici votre première page web</p>
            <p>Bravo !</p>
        </body>
    </html>
    ```
5. Enregistrer le fichier dans `1 – html – activité 1` sous le nom `index.html`
   
6. Ouvrir le fichier index.html avec un navigateur

### A rédiger
???+ help "Astuce"
    Utiliser les aides mémoire !

1. :pencil2: A quoi servent les balises suivantes 
    ``` html
    <title>
    <h3>
    <p>
    ```

2. :pencil2: Quel est le rôle de ces balises:
    ``` html
    <head>
    <body>
    ```
