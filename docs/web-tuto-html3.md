# 3 - Ajouter du images

### Objectif

[![Objectif html 3](img/web/html-obj3.png)](img/web/html-obj3.png)


### Démarche

1. Ouvrir le fichier index.html du répertoire `3 - html - activite 3` avec notepad++ et un navigateur
2. A l’aide de la balise `<img>`, ajouter les images sous les titre des thèmes correspondants 
   <br>:arrow_right: python.png 
   <br>:arrow_right: Internet.jpg
3. Enregistrer le fichier
4. Afficher le résultat dans le navigateur
5. Modifier la largeur des images pour qu’elles soient exactement de 200 pixels de largeur

!!! help "Astuce"
    Utiliser la balise `<img>`.
    C'est une balise orpheline. Inutile de la fermer.
    ``` html
    <img src="images/python.png" width="300">
    ```

 

### A rédiger

??? help "Astuce"
    Utiliser les aides mémoire !

1. :pencil2:Comment se nomme la balise permettant d’insérer une image ?
2. :pencil2:Comment se nomme l’attribut utiliser pour définir la largeur d’une image ?
3. :pencil2:Comment se nomme l’attribut permettant de définir l’emplacement de l’image à insérer ?



