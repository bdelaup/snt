# Balise HTML

### Structure d'une page
| **Balises**              | **Définitions**                                            |
| ------------------------ | ---------------------------------------------------------- |
| `<HTML>...</HTML>`       | Début et fin de fichier Html                               |
| `<HEAD>...</HEAD>`       | Zone d'en-tête d'un fichier Html (n’est pas affiché)       |
| `<TITLE>...</TITLE>`     | Titre de l’onglet affiché par le browser (élément de HEAD) |
| `<BODY>...</BODY>`       | Contient tous les éléments affichés                        |
| `<HEADER>...</HEADER>`   | Entête du document (contient éventuellement son titre)     |
| `<NAV>...</NAV>`         | Zone pour les liens permettant de naviguer sur le site     |
| `<MAIN>...</MAIN>`       | Zone de contenu principal                                  |
| `<SECTION>...</SECTION>` | Division en section du contenu                             |
| `<FOOTER>...</FOOTER>`   | Pied de page                                               |

| **Balises **                                                                                                                                                                                                                        | **Définitions**                                                         |
| ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------- |
| `<!--...-->`                                                                                                                                                                                                                        | Commentaire   (ignoré par le navigateur)                                |
| `<LINK   rel="stylesheet" href="styles/styles.css">`                                                                                                                                                                                | Utilise   une feuille de style pour la mise en forme                    |
| `<BR>`                                                                                                                                                                                                                              | Retour   à la ligne                                                     |
| `<P>...</P>`                                                                                                                                                                                                                        | Nouveau   paragraphe                                                    |
| `<h1>Titre   de niveau 1</h1>`<br>     `<h2>Titre   de niveau 2</h2>`<br>     `<h3>Titre   de niveau 3</h3>`<br>     `<h4>Titre   de niveau 4</h4>`<br>     `<h5>Titre   de niveau 5</h5>`<br>     `<h6>Titre de niveau 6</h6>`<br> | Niveau de   taille des titres :     du plus   gros(h1) à plus petit(h6) |
| `<HR>`                                                                                                                                                                                                                              | Barre   horizontale                                                     |
| `<EM>...</EM>`                                                                                                                                                                                                                      | Mettre en   valeur le texte (souvent en italique)                       |
| `<STRONG>...</STRONG> `                                                                                                                                                                                                             | Renforcer le   texte (souvent en gras)                                  |

### Les liens hypertextes

``` html 
<a href="https://www.techno.ovh">Le cours et le tuto sur techno.ovh</a>
 |   |       |                                |
 |   |       |                                Texte affiché sur la page web
 |   |       Cible du lien
 |   Attribut
 Balise
```


| **Balises**                         | **Définitions**                                          |
| ----------------------------------- | -------------------------------------------------------- |
| `<A href="http://wiki.org">...</A>` | Lien externe vers une page html d’un autre site Web      |
| `<A href="mailto: "...">...</A>`    | Lien qui démarre votre client de messagerie (outlook,….) |
| `<A href="rep/page.html">..</A> `   | Lien local vers une page du site.                        |

### Listes
#### Listes à puce
=== "Code"

    ``` html
    <UL>
        <LI> Lorem </LI>
        <LI> Ipsum </LI>
        <LI> Lorem </LI>
    </UL>  
    ```
=== "Rendu"
    
    <UL>
        <LI> Lorem </LI>
        <LI> Ipsum </LI>
    </UL>  
    

#### Liste numérotée
=== "code"

    ``` html
    <OL>
        <LI> lorem </LI>
        <LI> ipsum </LI>
    </OL>
    ```
=== "Rendu"

    <OL>
        <LI> lorem </LI>
        <LI> ipsum </LI>
    </OL>

### Tableau

| **Balises**          | **Définitions**         |
| -------------------- | ----------------------- |
| `<TABLE>...</TABLE>` | Définition d'un tableau |
| `<TR>...</TR>`       | Ligne du tableau        |
| `<TD>...</TD>`       | Cellule du tableau      |

=== "Code"

    ``` html
    <table>
        <TR> <TD>A1</TD> <TD>A2</TD> <TD>A3</TD> </TR>
        <TR> <TD>B1</TD> <TD>B2</TD> <TD>B3</TD> </TR>
        <TR> <TD>C1</TD> <TD>C2</TD> <TD>C3</TD> </TR>
    </table>
    ```
=== "Rendu"

    <table>
        <TR> <TD>...</TD> <TD>...</TD> <TD>...</TD> </TR>
        <TR> <TD>...</TD> <TD>...</TD> <TD>...</TD> </TR>
        <TR> <TD>...</TD> <TD>...</TD> <TD>...</TD> </TR>
    </table>

### Contenus multimédia
| Balises                                                                                               | Définitions                                                                            |
| ----------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------- |
| `<IMG   src="image/xyz.gif">`                                                                         | src : Attribut indiquant le chemin d’accès au fichier image.                           |
| `<IMG ... width=x height=y>`                                                                          | width, height : Largeur, hauteur en pixels                                             |
| `<IMG ...   alt="votre texte">`                                                                       | Alt : texte affiché pour les lecteur pour malvoyant                                    |
| `<VIDEO width="200" controls>` <br> `<SOURCE src="video/terre.mp4" type="video/mp4">` <br> `</VIDEO>` | <VIDEO width="200" controls><SOURCE src="../video/terre.mp4" type="video/mp4"></VIDEO> |
| `<AUDIO controls>` <br> `<SOURCE src="../sound/eau.wav" type="audio/wave">` <br> `</AUDIO>`           | <AUDIO controls><SOURCE src="../audio/eau.wav" type="audio/wav"></AUDIO>              |


