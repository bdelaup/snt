# Téléchargements

### Le tutoriel au format PDF

[![Tuto PDF](img/pdf-80.png)](assets/cours_tuto.pdf)


### Les fichiers nécessaires pour suivre le tuto

Procéder comme suit:

1. Télécharger l'archive [ici](assets/snt_tuto_html.zip)
2. Déplacer le fichier téléchargé dans l'espace personnel
3. Clique droit -> extraire ici

[![Tuto PDF](img/download.png)](assets/snt_tuto_html.zip)

