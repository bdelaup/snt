# 7 – Compléter la sémantique 




### Objectif

[![Objectif html 7](img/web/html-obj7.png){ width="400" }](img/web/html-obj7.png) 


### Démarche
Afin de facilité la mise en forme ultérieur, il nous faut identifié certaines zones.

*A faire :*

1. Ouvrir le fichier index.html du répertoire 6 - html - activite 6 avec notepad++ et un navigateur
2. Encadrer les section d’une balise `<main>`
3. Encadrer la zone de menu de la balise `<nav>`

!!! help "Astuce"
    Regarder dans l'aide-mémoire -> structure d'une page web pour comprendre comment disposer les balises `<nav> ... </nav>` et `<main> ... </main>`



